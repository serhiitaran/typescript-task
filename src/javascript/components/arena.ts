import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { IFighterDetails, Position, PositionArenaClassName } from '../typings/types';
import { showWinnerModal } from './modal/winner';

export function renderArena(selectedFighters: IFighterDetails[]) {
  const [firstFighter, secondFighter] = selectedFighters;
  const root: HTMLElement = document.getElementById('root')!;
  const arena: HTMLElement = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(firstFighter, secondFighter).then(fighter => showWinnerModal(fighter));
}

function createArena(selectedFighters: IFighterDetails[]): HTMLElement {
  const arena: HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
  const [firstFighter, secondFighter] = selectedFighters;
  const healthIndicators: HTMLElement = createHealthIndicators(firstFighter, secondFighter);
  const fighters: HTMLElement = createFighters(firstFighter, secondFighter);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails): HTMLElement {
  const healthIndicators: HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign: HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator: HTMLElement = createHealthIndicator(leftFighter, Position.left);
  const rightFighterIndicator: HTMLElement = createHealthIndicator(rightFighter, Position.right);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: Position): HTMLElement {
  const { name } = fighter;
  const container: HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName: HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` }
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighterDetails, secondFighter: IFighterDetails): HTMLElement {
  const battleField: HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement: HTMLElement = createFighter(firstFighter, Position.left);
  const secondFighterElement: HTMLElement = createFighter(secondFighter, Position.right);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: Position): HTMLElement {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const positionClassName: PositionArenaClassName =
    position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
