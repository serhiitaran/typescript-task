import {
  createArenaFighter,
  fightOptions,
  strikeByPlayerOne,
  strikeByPlayerTwo,
  criticalStrikeByPlayerOne,
  criticalStrikeByPlayerTwo,
  checkCombination,
  checkIsGameOver
} from '../helpers/fightHelper';
import { controls } from '../../constants/controls';
import { Position, IFighterDetails, IArenaFighter } from '../typings/types';

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails) {
  const playerOne: IArenaFighter = createArenaFighter(firstFighter, Position.left);
  const playerTwo: IArenaFighter = createArenaFighter(secondFighter, Position.right);
  const activeKeys: Set<string> = new Set<string>();

  return new Promise<IFighterDetails>(resolve => {
    document.addEventListener('keydown', fightHandler);
    document.addEventListener('keyup', fightHandler);

    function fightHandler(event: KeyboardEvent) {
      if (event.type === 'keydown') {
        activeKeys.add(event.code);
        setBlock();
      }
      if (event.type === 'keyup') {
        disableBlock(event.code);
        fightAction();
        activeKeys.delete(event.code);
      }
      const isGameOver: boolean = checkIsGameOver(playerOne, playerTwo);
      if (isGameOver) {
        document.removeEventListener('keydown', fightHandler);
        document.removeEventListener('keyup', fightHandler);
        const winner: IFighterDetails = playerOne.currentHealth <= 0 ? secondFighter : firstFighter;
        resolve(winner);
      }
    }

    function setBlock(): void {
      switch (true) {
        case activeKeys.has(controls.PlayerOneBlock):
          playerOne.block = true;
          break;
        case activeKeys.has(controls.PlayerTwoBlock):
          playerTwo.block = true;
          break;
      }
    }

    function disableBlock(key: string): void {
      switch (key) {
        case controls.PlayerOneBlock:
          playerOne.block = false;
          break;
        case controls.PlayerTwoBlock:
          playerTwo.block = false;
          break;
      }
    }

    function fightAction(): void {
      switch (true) {
        case checkCombination(fightOptions.bothStrike, activeKeys):
          strikeByPlayerOne(playerOne, playerTwo);
          strikeByPlayerTwo(playerOne, playerTwo);
          break;
        case checkCombination(fightOptions.oneCriticalStrikeTwoStrike, activeKeys):
          criticalStrikeByPlayerOne(playerOne, playerTwo);
          strikeByPlayerTwo(playerOne, playerTwo);
          break;
        case checkCombination(fightOptions.oneStrikeTwoCriticalStrike, activeKeys):
          strikeByPlayerOne(playerOne, playerTwo);
          criticalStrikeByPlayerTwo(playerOne, playerTwo);
          break;
        case checkCombination(fightOptions.bothCriticalStrike, activeKeys):
          criticalStrikeByPlayerOne(playerOne, playerTwo);
          criticalStrikeByPlayerTwo(playerOne, playerTwo);
          break;
        case activeKeys.has(controls.PlayerOneAttack):
          strikeByPlayerOne(playerOne, playerTwo);
          break;
        case activeKeys.has(controls.PlayerTwoAttack):
          strikeByPlayerTwo(playerOne, playerTwo);
          break;
        case checkCombination(controls.PlayerOneCriticalHitCombination, activeKeys):
          criticalStrikeByPlayerOne(playerOne, playerTwo);
          break;
        case checkCombination(controls.PlayerTwoCriticalHitCombination, activeKeys):
          criticalStrikeByPlayerTwo(playerOne, playerTwo);
          break;
      }
    }
  });
}
