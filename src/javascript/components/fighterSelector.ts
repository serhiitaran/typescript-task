import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { SelectedFighter, IFighterDetails, Position } from '../typings/types';
import versusImg from '../../../resources/versus.png';

export function createFightersSelector() {
  let selectedFighters: SelectedFighter[] = [];

  return async (_event: Event, fighterId: string): Promise<void> => {
    const fighter: IFighterDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: SelectedFighter = playerOne ?? fighter;
    const secondFighter: SelectedFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    renderSelectedFighters(selectedFighters as IFighterDetails[]);
  };
}

const fighterDetailsMap = new Map<string, IFighterDetails>();

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  const hasFighter: boolean = fighterDetailsMap.has(fighterId);
  if (!hasFighter) {
    const fighterDetails: IFighterDetails = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterDetails);
  }
  return fighterDetailsMap.get(fighterId) as IFighterDetails;
}

function renderSelectedFighters(selectedFighters: IFighterDetails[]): void {
  const fightersPreview: HTMLElement = document.querySelector('.preview-container___root') as HTMLElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, Position.left);
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, Position.right);
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighterDetails[]): HTMLElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: String(versusImg) }
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFighterDetails[]): void {
  renderArena(selectedFighters);
}
