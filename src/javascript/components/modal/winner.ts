import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { IFighterDetails } from '../../typings/types';

export function showWinnerModal(fighter: IFighterDetails): void {
  const title: string = `${fighter.name} is win!`;
  const bodyElement: HTMLElement = createFighterImage(fighter);
  const onClose = (): void => window.location.reload();
  showModal({ title, bodyElement, onClose });
}
