import { IFighterDetails, IArenaFighter, IFightOptions, Position } from '../typings/types';
import { controls } from '../../constants/controls';
import { CRITICAL_HIT_DELAY } from '../../constants/fight';

export function createArenaFighter(fighter: IFighterDetails, position: Position): IArenaFighter {
  return {
    ...fighter,
    currentHealth: fighter.health,
    block: false,
    indicator: position === 'right' ? '#right-fighter-indicator' : '#left-fighter-indicator',
    canStrikeCritical: true
  };
}

export function strikeByPlayerOne(playerOne: IArenaFighter, playerTwo: IArenaFighter): void {
  strike(playerOne, playerTwo);
}

export function strikeByPlayerTwo(playerOne: IArenaFighter, playerTwo: IArenaFighter): void {
  strike(playerTwo, playerOne);
}

export function criticalStrikeByPlayerOne(playerOne: IArenaFighter, playerTwo: IArenaFighter): void {
  strikeCritical(playerOne, playerTwo);
}

export function criticalStrikeByPlayerTwo(playerOne: IArenaFighter, playerTwo: IArenaFighter): void {
  strikeCritical(playerTwo, playerOne);
}

export const fightOptions: IFightOptions = {
  bothStrike: [controls.PlayerOneAttack, controls.PlayerTwoAttack],
  oneStrikeTwoCriticalStrike: [controls.PlayerOneAttack, ...controls.PlayerTwoCriticalHitCombination],
  oneCriticalStrikeTwoStrike: [...controls.PlayerOneCriticalHitCombination, controls.PlayerTwoAttack],
  bothCriticalStrike: [...controls.PlayerOneCriticalHitCombination, ...controls.PlayerTwoCriticalHitCombination]
};

export function checkCombination(combination: string[], activeKeys: Set<string>): boolean {
  for (let key of combination) {
    if (!activeKeys.has(key)) {
      return false;
    }
  }
  return true;
}

export function checkIsGameOver(playerOne: IArenaFighter, playerTwo: IArenaFighter): boolean {
  if (playerOne.currentHealth <= 0 || playerTwo.currentHealth <= 0) {
    return true;
  } else {
    return false;
  }
}

function strike(attacker: IArenaFighter, defender: IArenaFighter): void {
  if (!attacker.block && !defender.block) {
    defender.currentHealth -= getDamage(attacker, defender);
    changeHealthIndicator(defender);
  }
}

function strikeCritical(attacker: IArenaFighter, defender: IArenaFighter): void {
  if (attacker.canStrikeCritical) {
    defender.currentHealth -= getCriticalHitPower(attacker.attack);
    changeHealthIndicator(defender);
    frezzeCriticalHit(attacker);
  }
}

function getCriticalHitPower(attack: number): number {
  return attack * 2;
}

function frezzeCriticalHit(fighter: IArenaFighter): void {
  fighter.canStrikeCritical = false;
  setTimeout(() => (fighter.canStrikeCritical = true), CRITICAL_HIT_DELAY);
}

function getDamage(attacker: IArenaFighter, defender: IArenaFighter): number {
  const damage: number = getHitPower(attacker.attack) - getBlockPower(defender.defense);
  return damage > 0 ? damage : 0;
}

function getHitPower(attack: number): number {
  const criticalHitChance: number = getRandomNum(1, 2);
  return attack * criticalHitChance;
}

function getBlockPower(defense: number): number {
  const dodgeChance: number = getRandomNum(1, 2);
  return defense * dodgeChance;
}

function getRandomNum(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}

function changeHealthIndicator(player: IArenaFighter): void {
  const { health, currentHealth, indicator } = player;
  const lowHealth: number = 25;
  const lowHealthIndicatorColor: string = '#ba0303';
  const defaultIdicatorColor: string = '#ebd759';
  const playerIndicator: HTMLElement = document.querySelector(indicator) as HTMLElement;
  const indicatorValue: number = (currentHealth / health) * 100;
  const indicatorColor: string = indicatorValue <= lowHealth ? lowHealthIndicatorColor : defaultIdicatorColor;
  playerIndicator.style.width = indicatorValue > 0 ? indicatorValue + '%' : '0';
  playerIndicator.style.backgroundColor = indicatorColor;
}
