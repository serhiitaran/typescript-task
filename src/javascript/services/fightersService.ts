import { callApi } from '../helpers/apiHelper';
import { IFighter, IFighterDetails } from '../typings/types';

class FighterService {
  async getFighters() {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: IFighter[] = (await callApi(endpoint, 'GET')) as IFighter[];

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult: IFighterDetails = (await callApi(endpoint, 'GET')) as IFighterDetails;

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
