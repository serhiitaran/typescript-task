export type ApiResponse = IFighter[] | IFighterDetails;

export type SelectedFighter = undefined | IFighterDetails;

export type SelectFighter = { (_event: Event, fighterId: string): Promise<void> };

export type PositionPreviewClassName = 'fighter-preview___right' | 'fighter-preview___left';

export type PositionArenaClassName = 'arena___right-fighter' | 'arena___left-fighter';

export type ArenaFighterIndicator = '#right-fighter-indicator' | '#left-fighter-indicator';

export type CreateModal = { title: string; bodyElement: HTMLElement; onClose: () => void };

export enum Position {
  right = 'right',
  left = 'left'
}

export interface IFighter {
  _id: string;
  name: string;
  source: any;
}

export interface IFighterDetails extends IFighter {
  health: number;
  attack: number;
  defense: number;
}

export interface IArenaFighter extends IFighterDetails {
  currentHealth: number;
  block: boolean;
  indicator: ArenaFighterIndicator;
  canStrikeCritical: boolean;
}

export interface IFetchOptions {
  method: string;
}

export interface ICreateElementParams {
  tagName: string;
  className?: string;
  attributes?: { [key: string]: string };
}

export interface IFightControls {
  PlayerOneAttack: string;
  PlayerOneBlock: string;
  PlayerTwoAttack: string;
  PlayerTwoBlock: string;
  PlayerOneCriticalHitCombination: string[];
  PlayerTwoCriticalHitCombination: string[];
}

export interface IFightOptions {
  bothStrike: string[];
  oneStrikeTwoCriticalStrike: string[];
  oneCriticalStrikeTwoStrike: string[];
  bothCriticalStrike: string[];
}
